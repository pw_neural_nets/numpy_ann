import numpy as np

eps = 1e-15


class ActivationFunctions:
    @staticmethod
    def exec(activation, Z_curr):
        if activation == "relu":
            activation_func = ActivationFunctions.relu
        elif activation == "sigmoid":
            activation_func = ActivationFunctions.sigmoid
        elif activation == "softmax":
            activation_func = ActivationFunctions.softmax
        elif activation == "linear":
            activation_func = ActivationFunctions.linear
        elif activation == "leaky_relu":
            activation_func = ActivationFunctions.leaky_relu
        elif activation == "tanh":
            activation_func = ActivationFunctions.tanh
        else:
            raise Exception(f"This activation function is not supported: {activation}")
        return activation_func(Z_curr)

    @staticmethod
    def exec_backward(activation, dA_curr, Z_curr, Y_hat, Y):
        if activation == "relu":
            backward_activation_function = ActivationFunctions.relu_backward
            arguments = (dA_curr, Z_curr)
        elif activation == "sigmoid":
            backward_activation_function = ActivationFunctions.sigmoid_backward
            arguments = (dA_curr, Z_curr)
        elif activation == "softmax":
            backward_activation_function = ActivationFunctions.softmax_backward
            arguments = (Y_hat, Y)
        elif activation == "linear":
            backward_activation_function = ActivationFunctions.linear_backward
            arguments = (dA_curr, Z_curr)
        elif activation == "leaky_relu":
            backward_activation_function = ActivationFunctions.leaky_relu_backward
            arguments = (dA_curr, Z_curr)
        elif activation == "tanh":
            backward_activation_function = ActivationFunctions.tanh_backward
            arguments = (dA_curr, Z_curr)
        else:
            raise Exception(f'This activation function is not supported: {activation}')
        return backward_activation_function(*arguments)

    @staticmethod
    def sigmoid(Z):
        return 1 / (1 + np.exp(-Z))

    @staticmethod
    def sigmoid_backward(dA, Z):
        sig = ActivationFunctions.sigmoid(Z)
        return dA * sig * (1 - sig)

    @staticmethod
    def relu(Z):
        return np.maximum(0, Z)

    @staticmethod
    def relu_backward(dA, Z):
        dZ = np.array(dA, copy=True)
        dZ[Z <= 0] = 0
        return dZ

    @staticmethod
    def leaky_relu(Z):
        Z = ((Z > 0) * Z) + ((Z < 0) * Z * 0.01)
        return Z

    @staticmethod
    def leaky_relu_backward(dA, Z):
        lrb = np.zeros(Z.shape)
        lrb[Z > 0] = 1
        lrb[Z < 0] = 0.01
        return dA * lrb

    @staticmethod
    def softmax(Z):
        return np.exp(Z) / sum(np.exp(Z))

    @staticmethod
    def softmax_backward(Y_hat, Y):
        return Y_hat - Y

    @staticmethod
    def linear(Z):
        return Z

    @staticmethod
    def linear_backward(dA, Z):
        return dA * np.ones_like(Z)

    @staticmethod
    def tanh(Z):
        return np.tanh(Z)

    @staticmethod
    def tanh_backward(dA, Z):
        thb = 1 - ActivationFunctions.tanh(Z)**2
        return dA * thb