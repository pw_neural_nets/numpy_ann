import preprocessing
import numpy as np


def encode(model, X, Y):
    """Encodes dataset to perform forward propagation.
    
    Args:
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
        Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
    
    Returns:
        (
            array: X encoded.
            array: Y encoded.
        )
    """

    if hasattr(model, "mode") and model.mode is not None:
        if model.mode == "classification":
            if Y.shape[0] == 1:
                return BinaryClassificationMode.encode(model, X, Y)
            elif Y.shape[0] > 1:
                return MultipleClassificationMode.encode(model, X, Y)
        elif model.mode == "binary_classification":
            return BinaryClassificationMode.encode(model, X, Y)
        elif model.mode == "multiple_classification":
            return MultipleClassificationMode.encode(model, X, Y)
        elif model.mode == "regression":
            return RegressionMode.encode(model, X, Y)

    return X, Y

def decode(model, X, Y):
    """Decodes dataset.
    
    Args:
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
        Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
    
    Returns:
        (
            array: X decoded.
            array: Y decoded.
        )
    """

    if hasattr(model, "mode"):
        if model.mode == "classification":
            if Y.shape[0] == 1:
                return BinaryClassificationMode.decode(model, X, Y)
            elif Y.shape[0] > 1:
                return MultipleClassificationMode.decode(model, X, Y)
        elif model.mode == "binary_classification":
            return BinaryClassificationMode.decode(model, X, Y)
        elif model.mode == "multiple_classification":
            return MultipleClassificationMode.decode(model, X, Y)
        elif model.mode == "regression":
            return RegressionMode.decode(model, X, Y)

    return X, Y
    

def load_mode(mode_name, model, X=None, Y=None, seed=2):
    """Loads MLP training mode.
    
    Args:
        mode_name (string): Mode name.
        model (MLP): MLP instance.
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
        Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
    
    Raises:
        Exception: Raised in case of unsupported mode or mode determination error.
    
    Returns:
        (
            array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes. 
            array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
        )
    """

    if mode_name == "regression":
        model.mode = "regression"
        return RegressionMode.load(model, X, Y, seed)
    elif mode_name == "classification":
        if Y is None:
            raise Exception("Couldn't determine mode. Y is obligatory.")
        else:
            unique_values = np.unique(Y)
            if len(unique_values) == 2:
                model.mode = "binary_classification"
                return BinaryClassificationMode.load(model, X, Y, seed)
            elif len(unique_values) > 2:
                model.mode = "multiple_classification"
                return MultipleClassificationMode.load(model, X, Y, seed)
            else:
                raise Exception(f"Couldn't determine mode. Number of unique values of Y is: {len(unique_values)}.")
    elif mode_name == "binary_classification":
        model.mode = "binary_classification"
        return BinaryClassificationMode.load(model, X, Y, seed)
    elif mode_name == "multiple_classification":
        model.mode = "multiple_classification"
        return MultipleClassificationMode.load(model, X, Y, seed)
    else:
        raise Exception(f"This mode is not supported: {mode_name}")
    
class RegressionMode:

    @staticmethod
    def encode(model, X, Y):
        """Encodes dataset to perform forward propagation.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X encoded.
                array: Y encoded.
            )
        """

        X_prepared = model.X_scaler.transform(X) if (X is not None) else X
        Y_prepared = model.Y_scaler.transform(Y) if (Y is not None) else Y

        return X_prepared, Y_prepared

    @staticmethod
    def decode(model, X, Y):
        """Decodes dataset.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X decoded.
                array: Y decoded.
            )
        """

        X_scaler = model.X_scaler
        Y_scaler = model.Y_scaler
        X, Y = X_scaler.inverse_transform(X), Y_scaler.inverse_transform(Y)

        return X, Y

    @staticmethod
    def load(model, X=None, Y=None, seed=2):
        """Loads MLP training mode.
        
        Args:
            mode_name (string): Mode name.
            model (MLP): MLP instance.
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
        
        Raises:
            Exception: Raised in case of unsupported mode or mode determination error.
        
        Returns:
            (
                array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes. 
                array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
            )
        """

        model.loss = "mean_squared_error"
        model.architecture[-1]['activation'] = "linear"
        if X is not None and Y is not None:
            X_shuffled = preprocessing.shuffle(X, seed)
            Y_shuffled = preprocessing.shuffle(Y, seed)
            X_scaler = preprocessing.MinMaxScaler()
            X_scaler.fit(X_shuffled)
            Y_scaler = preprocessing.MinMaxScaler()
            Y_scaler.fit(Y_shuffled)
            model.X_scaler = X_scaler
            model.Y_scaler = Y_scaler
            X, Y = RegressionMode.encode(model, X_shuffled, Y_shuffled)

        return X, Y
    
class BinaryClassificationMode:
    
    @staticmethod
    def encode(model, X, Y):
        """Encodes dataset to perform forward propagation.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X encoded.
                array: Y encoded.
            )
        """
        X, Y = X, preprocessing.one_hot_encoding(Y)
        return X, Y

    @staticmethod
    def decode(model, X, Y):
        """Decodes dataset.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X decoded.
                array: Y decoded.
            )
        """

        X, Y = X, np.vectorize(model.label_decoder)(np.round(Y).astype(int))
        return X, Y

    @staticmethod
    def load(model, X=None, Y=None, seed=2):
        """Loads MLP training mode.
        
        Args:
            mode_name (string): Mode name.
            model (MLP): MLP instance.
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
        
        Raises:
            Exception: Raised in case of unsupported mode or mode determination error.
        
        Returns:
            (
                array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes. 
                array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
            )
        """

        model.loss = "mean_squared_error"
        model.architecture[-1]['activation'] = "sigmoid"
        if X is not None and Y is not None:
            X_shuffled = preprocessing.shuffle(X, seed)
            unique_values = np.unique(Y)
            model.label_decoder = lambda label: unique_values[label]
            Y_shuffled = preprocessing.shuffle(Y, seed)
            X, Y = BinaryClassificationMode.encode(model, X_shuffled, Y_shuffled)

        return X, Y
    
class MultipleClassificationMode:
    
    @staticmethod
    def encode(model, X, Y):
        """Encodes dataset to perform forward propagation.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X encoded.
                array: Y encoded.
            )
        """

        X, Y = X, preprocessing.one_hot_encoding(Y)

        return X, Y

    @staticmethod
    def decode(model, X, Y):
        """Decodes dataset.
        
        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
        
        Returns:
            (
                array: X decoded.
                array: Y decoded.
            )
        """

        X, Y = X, np.apply_along_axis(model.label_decoder, 0, Y).reshape(1,-1)

        return X, Y

    @staticmethod
    def load(model, X=None, Y=None, seed=2):
        """Loads MLP training mode.
        
        Args:
            mode_name (string): Mode name.
            model (MLP): MLP instance.
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
        
        Raises:
            Exception: Raised in case of unsupported mode or mode determination error.
        
        Returns:
            (
                array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes. 
                array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
            )
        """
        
        model.loss = "categorical_crossentropy"
        model.architecture[-1]['activation'] = "softmax"
        model.label_decoder = lambda label: unique_values[np.argmax(label)]
        if X is not None and Y is not None:
            X_shuffled = preprocessing.shuffle(X, seed)
            Y_shuffled = preprocessing.shuffle(Y, seed)
            unique_values = np.unique(Y)
            model.architecture[-1]['output_dim'] = len(unique_values)
            model.label_decoder = lambda label: unique_values[np.argmax(label)]
            X, Y = MultipleClassificationMode.encode(model, X_shuffled, Y_shuffled)

        return X, Y
            
        