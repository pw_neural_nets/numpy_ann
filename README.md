# MLP implementation 

Project for classes of Deep Learning at Warsaw University of Technology. Implementation of Multilayer Perceptron core is available in mlp.py file.

# Report

Report is available in report.md file.

# Visualisations of learning process

## Binary classification

<img src="binary_classification.gif" />

## Multiple Classification

<img src="multiple_classification.gif" />

## Regression

<img src="regression.gif" />