import numpy as np

class MinMaxScaler:
    def __init__(self):
        pass

    def fit(self, X, axis = 1, min_v=0, max_v=1):
        self.min_v = min_v
        self.max_v = max_v
        self.axis = axis
        self.X_min = X.min(axis=axis)
        self.X_max = X.max(axis=axis)

    def transform(self, X):
        return (X - self.X_min) / (self.X_max - self.X_min) * (self.max_v - self.min_v) + self.min_v

    def inverse_transform(self, Y):
        return (Y - self.min_v) / (self.max_v - self.min_v) * (self.X_max - self.X_min) + self.X_min
    
class StandardScaler:
    def __init__(self):
        pass

    def fit(self, X, axis = 1):
        self.mean_X = np.mean(X)
        self.std_X = np.std(X)
        self.axis = axis

    def transform(self, X):
        return (X - self.mean_X) / self.std_X

    def inverse_transform(self, Y):
        return Y * self.std_X + self.mean_X
    
def shuffle(X, seed):
    indices = list(range(X.shape[1]))
    np.random.RandomState(seed).shuffle(indices)
    return X[:,indices]


def one_hot_encoding(Y):
    unique_values = np.unique(Y)
    if len(unique_values) <= 2:
        label_encoder = {value: np.where(unique_values == value)[0][0] for value in unique_values}
        Y = np.vectorize(label_encoder.get)(Y)
    elif len(unique_values) > 2:
        label_encoder = {value: np.where(unique_values == value)[0][0] for value in unique_values}
        Y = np.vectorize(label_encoder.get)(Y)
        Y = np.eye(len(unique_values))[:,Y.reshape(-1)]
    return Y