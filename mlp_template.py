import modes
import numpy as np
import math
from loss_functions import LossFunctions
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from PIL import Image
import glob
import io
from preprocessing import MinMaxScaler
from copy import deepcopy

class MLPTemplate:
    def __init__(
            self,
            X,
            Y,
            bias=True,
            batch_size=32,
            iterations=None,
            learning_rate=0.01,
            momentum_rate=0.9,
            mode=None,
            loss=None,
            epochs=100,
            verbose=False,
            seed=2,
            early_stopping=False,
            patience=3,
            validation_split=None,
    ):
        """Describes model architecture and options.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in model training and optionally for validation purposes.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in model training and optionally for validation purposes.
            bias (bool, optional): Presence of biases in layers. Defaults to True.
            batch_size (int, optional): Size of training sample in single iteration. Defaults to 32.
            iterations (int, optional): Maximum value of iterations. Defaults to None.
            learning_rate (float, optional): Learning rate. Defaults to 0.01.
            momentum_rate (float, optional): Momentum rate. Defaults to 0.9.
            mode (string, optional): Mode used in training.
                Supported values: "regression", "classification", "binary_classification", "multiple_classification".
                It results in loss attribute override, architecture adjustments and data preprocessing.
                Defaults to None.
            loss (str, optional): Loss function.
                Supported values: "binary_crossentropy", "categorical_crossentropy", "mean_squared_error".
                Defaults to "binary_crossentropy".
            epochs (int, optional): Number of epochs of training process. Defaults to 100.
            verbose (bool, optional): Training process verbosity. Defaults to False.
            seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
            early_stopping (bool, optional): Defines if early stopping technique is used.. Defaults to False.
            patience (int, optional): Early stopping patience parameter.
                Represents a maximum number of epochs that model `waits` for validation loss improvement.
                Defaults to 3.
            validation_split (float, optional): Training size ratio. Defaults to None.
        """

        self.load_attributes(X, Y, bias, batch_size, iterations, learning_rate, momentum_rate, mode, loss, seed)

    def load_attributes(self, X, Y, bias, batch_size, iterations, learning_rate, momentum_rate, mode, loss, seed):
        """Loads model attributes.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in model training and optionally for validation purposes.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in model training and optionally for validation purposes.
            bias (bool, optional): Presence of biases in layers. Defaults to True.
            batch_size (int, optional): Size of training sample in single iteration. Defaults to 32.
            iterations (int, optional): Maximum value of iterations. Defaults to None.
            learning_rate (float, optional): Learning rate. Defaults to 0.01.
            momentum_rate (float, optional): Momentum rate. Defaults to 0.9.
            mode (string, optional): Mode used in training.
                Supported values: "regression", "classification", "binary_classification", "multiple_classification".
                It results in loss attribute override, architecture adjustments and data preprocessing.
                Defaults to None.
            loss (str, optional): Loss function.
                Supported values: "binary_crossentropy", "categorical_crossentropy", "mean_squared_error".
                Defaults to "binary_crossentropy".
            epochs (int, optional): Number of epochs of training process. Defaults to 100.
            verbose (bool, optional): Training process verbosity. Defaults to False.
            seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.

        Returns:
            (
                array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes.
                array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
            )
        """

        if mode is not None:
            X_prepared, Y_prepared = modes.load_mode(mode, self, X, Y, seed)
            self.bias = bias
            self.batch_size = batch_size
            self.iterations = iterations
            self.learning_rate = learning_rate
            self.momentum_rate = momentum_rate
            if loss is not None:
                self.loss = loss
        else:
            X_prepared, Y_prepared = X, Y
            self.bias = bias
            self.batch_size = batch_size
            self.iterations = iterations
            self.learning_rate = learning_rate
            self.momentum_rate = momentum_rate
            self.mode = mode
            self.loss = loss

        return X_prepared, Y_prepared

    def split_training_set(self, X, Y):
        """Splits training set into batches.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in model training and optionally for validation purposes.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in model training and optionally for validation purposes.

        Returns:(
            list: Batches of X.
            list: Batches of Y.
        )
        """

        number_of_batches = math.ceil(X.shape[1] / self.batch_size)
        X_batches = np.array_split(X, number_of_batches, axis=1)
        Y_batches = np.array_split(Y, number_of_batches, axis=1)

        return X_batches, Y_batches

    def get_loss_value(self, Y_hat, Y):
        """Calculates loss function value.

        Args:
            Y_hat (array): Outputs generated by the Multilayer Perceptron.
            Y (array): Real outputs.

        Raises:
            Exception: Raised in case of unsupported loss function.

        Returns:
            float: Loss function value.
        """

        loss = self.loss
        if loss == "binary_crossentropy":
            loss_value = LossFunctions.binary_crossentropy(Y_hat, Y)
        elif loss == "categorical_crossentropy":
            loss_value = LossFunctions.categorical_crossentropy(Y_hat, Y)
        elif loss == "mean_squared_error":
            loss_value = LossFunctions.mean_squared_error(Y_hat, Y)
        elif loss == "root_mean_squared_error":
            loss_value = LossFunctions.root_mean_squared_error(Y_hat, Y)
        else:
            raise Exception(f"This loss function is not supported: {loss}")

        return np.squeeze(loss_value)

    def get_accuracy_value(self, Y_hat, Y):
        """Calculates accuracy function value.

        Args:
            Y_hat (array): Outputs generated by the Multilayer Perceptron.
            Y (array): Real outputs.

        Returns:
            float: Accuracy function value.
        """

        mode = self.mode
        if mode in ("classification", "binary_classification", "multiple_classification"):
            Y_hat_ = np.round(Y_hat)
            accuracy_value = (Y_hat_ == Y).all(axis=0).mean()
        elif mode == "regression":
            accuracy_value = LossFunctions.root_mean_squared_error(Y_hat, Y)
        else:
            accuracy_value = None

        return accuracy_value

    def validate_sets(self, X, Y):
        """Performs data validation.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in model training and optionally for validation purposes.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in model training and optionally for validation purposes.

        Raises:
            Exception: Raised in case of unsupported Y shape.

        Returns:
            [type]: [description]
        """

        if (Y.shape[0] != 1):
            raise Exception("The only supported shape of Y is (1,-1).")
        else:
            return True

    def init_history_and_cache(self):
        """Initialize history and cache attributes.
        """

        self.parameters_history = []
        self.train_loss_history = []
        self.train_accuracy_history = []
        self.val_loss_history = []
        self.val_accuracy_history = []
        self.best_val_epoch = -1
        self.cache = {}

    def dataset_split(self, X_prepared, Y_prepared, validation_split):
        """Performs dataset split based on given ratio.

        Args:
            X_prepared (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes.
            Y_prepared (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
            validation_split (float): Percentage of training set.

        Returns:
            (
                array: X_prepared subset for training purposes.
                array: Y_prepared subset for training purposes.
                array: X_prepared subset for validation purposes.
                array: Y_prepared subset for validation purposes.
            )
        """

        if validation_split is not None:
            X_train_prepared, X_val_prepared = np.split(X_prepared, [int(X_prepared.shape[1] * validation_split)], axis=1)
            Y_train_prepared, Y_val_prepared = np.split(Y_prepared, [int(Y_prepared.shape[1] * validation_split)], axis=1)
        else:
            X_train_prepared = X_prepared
            Y_train_prepared = Y_prepared
            X_val_prepared = None
            Y_val_prepared = None

        return X_train_prepared, Y_train_prepared, X_val_prepared, Y_val_prepared

    def print_epoch_report(self, verbose, train_loss_value, train_accuracy_value, val_loss_value, val_accuracy_value, epoch):
        """Prints epoch report.

        Args:
            verbose (bool): Verbosity of training process.
            train_loss_value (float): Loss function value on a whole training set.
            train_accuracy_value (float): Accuracy function value on a whole training set.
            val_loss_value (float): Loss function value on a whole validation set.
            val_accuracy_value (float): Accuracy function value on a whole validation set.
            epoch (int): Number of epoch.
        """
        if (verbose):
            train_loss_comp = f" - train loss: {float(train_loss_value):.10f}" if train_loss_value else ""
            train_acc_comp = f" - train accuracy: {float(train_accuracy_value):.10f}" if train_accuracy_value else ""
            val_loss_comp = f" - val loss: {float(val_loss_value):.10f}" if val_loss_value else ""
            val_acc_comp = f" - val accuracy: {float(val_accuracy_value):.10f}" if val_accuracy_value else ""
            print(f"Epoch: {epoch+1:05} {train_loss_comp}{train_acc_comp}{val_loss_comp}{val_acc_comp}")

    def encode(self, X, Y):
        """Encodes dataset to perform forward propagation.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.

        Returns:
        (
            array: X encoded.
            array: Y encoded.
        )
        """

        return modes.encode(self, X, Y)

    def decode(self, X, Y):
        """Decodes dataset.

        Args:
            X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.

        Returns:
        (
            array: X decoded.
            array: Y decoded.
        )
        """

        return modes.decode(self, X, Y)

    def load_best_parameters(self):
        """Loads best parameters based on validation loss values.
        """

        if self.best_val_epoch != -1:
            self.parameters_values = self.parameters_history[self.best_val_epoch]
        else:
            self.parameters_values = self.parameters_history[-1]
            print("Best paremeters are not defined. Loaded most recent parameters.")

    def create_2D_visualisation(self, X_train, Y_train):
        current_parameters_values = deepcopy(self.parameters_values)
        if self.mode in ("classification", "binary_classification", "multiple_classification"):
            frames = []
            buffer = io.BytesIO()
            epochs = (lambda m, n: [i*n//m + n//(2*m) for i in range(m)])(100, len(self.parameters_history))
            for epoch in epochs:
                self.parameters_values = self.parameters_history[epoch]
                plt.figure(figsize=(8, 8))
                sections = 50

                x_min, x_max = X_train[0, :].min(), X_train[0, :].max()
                y_min, y_max = X_train[1, :].min(), X_train[1, :].max()

                x_padding = (x_max - x_min) / 10
                y_padding = (y_max - y_min) / 10

                x1, x2 = np.meshgrid(np.linspace(x_min - x_padding, x_max + x_padding, sections), np.linspace(y_min - y_padding, y_max + y_padding, sections))

                x1_ravel = x1.ravel()
                x2_ravel = x2.ravel()

                Z = self.predict(np.c_[x1_ravel, x2_ravel].T, np.zeros((1, x1_ravel.shape[0])), verbose=False).reshape(x1.shape)

                buffer = io.BytesIO()

                ax = plt.subplot(1, 1, 1)
                ax.set_title("Classification of training data.")
                cs = ax.contourf(x1, x2, Z, cmap=plt.get_cmap("Spectral"), alpha=.8)
                ax.scatter(X_train[0, :], X_train[1, :], c=Y_train.squeeze(), cmap=plt.get_cmap("Spectral"), edgecolors='k', alpha=.9)
                ax.set_xlim(x1.min(), x1.max())
                ax.set_ylim(x2.min(), x2.max())
                ax.set_xlabel("X1")
                ax.set_ylabel("X2")
                ax.set_xticks(())
                ax.set_yticks(())
                
                props = dict(boxstyle='round', facecolor='wheat', alpha=.8)
                plt.text(x_min - x_padding / 2, y_max + y_padding / 2, f"Epoch: {epoch}\nLoss: {self.train_loss_history[epoch-1]:.5f}\nAccuracy: {self.train_accuracy_history[epoch-1]:.5f}", verticalalignment='top', bbox=props, fontsize=14)
                
                plt.savefig(buffer, dpi=80)
                buffer.seek(0)
                image = Image.open(buffer)
                frames.append(image)
                
                plt.close('all')

            if frames:
                frames[0].save(f"{self.mode}.gif", format='GIF', append_images=frames[1:], optimize=True, save_all=True, duration=10000 / len(frames), loop=0)

            buffer.close()

        elif self.mode == "regression":
            frames = []
            buffer = io.BytesIO()
            epochs = (lambda m, n: [i*n//m + n//(2*m) for i in range(m)])(100, len(self.parameters_history))
            for epoch in epochs:
                self.parameters_values = self.parameters_history[epoch]
                plt.figure(figsize=(8, 8))
                sections = 1000

                x_min, x_max = X_train[0, :].min(), X_train[0, :].max()
                y_min, y_max = Y_train[0, :].min(), Y_train[0, :].max()

                x_padding = (x_max - x_min) / 5
                y_padding = (y_max - y_min) / 5

                x = np.linspace(x_min - x_padding, x_max + x_padding, sections).reshape(1,-1)

                Y_hat = self.predict(x, np.zeros(x.shape), verbose=False)

                buffer = io.BytesIO()

                ax = plt.subplot(1, 1, 1)
                ax.set_title("Regression of training data.")
                ax.plot(x.squeeze(), Y_hat[0, :], color="red")
                ax.scatter(X_train[0, :], Y_train[0, :], c="grey", edgecolors='k', alpha=.5)
                ax.set_xlim(x_min-x_padding, x_max+x_padding)
                ax.set_ylim(y_min-y_padding, y_max+y_padding)
                ax.set_xlabel("X1")
                ax.set_ylabel("Y")
                props = dict(boxstyle='round', facecolor='wheat', alpha=.8)
                plt.text(x_min - x_padding / 2, y_max + y_padding / 2, f"Epoch: {epoch}\nLoss: {self.train_loss_history[epoch-1]:.5f}", verticalalignment='top', bbox=props, fontsize=14)
                
                plt.savefig(buffer, dpi=80)
                buffer.seek(0)
                image = Image.open(buffer)
                frames.append(image)
                
                plt.close('all')

            if frames:
                frames[0].save(f"{self.mode}.gif", format='GIF', append_images=frames[1:], optimize=True, save_all=True, duration=10000 / len(frames), loop=0)

            buffer.close()

        self.parameters_values = current_parameters_values

    def plot_loss(self, X_train=None, Y_train=None, X_test=None, Y_test=None, title="image"):

        current_parameters_values = deepcopy(self.parameters_values)

        plt.figure(figsize=(8, 8))
        ax = plt.subplot(1, 1, 1)
        
        train_loss_values = []
        for parameter_values in self.parameters_history:
            X_train_encoded, Y_train_encoded = self.encode(X_train, Y_train)
            self.parameters_values = parameter_values
            Y_train_hat = self.forward_propagation(X_train_encoded)
            train_loss_value = self.get_loss_value(Y_train_hat, Y_train_encoded)
            train_loss_values.append(train_loss_value)
        ax.plot(range(len(train_loss_values)), train_loss_values, label="Train")

        test_loss_values = []
        for parameter_values in self.parameters_history:
            X_test_encoded, Y_test_encoded = self.encode(X_test, Y_test)
            self.parameters_values = parameter_values
            Y_test_hat = self.forward_propagation(X_test_encoded)
            test_loss_value = self.get_loss_value(Y_test_hat, Y_test_encoded)
            test_loss_values.append(test_loss_value)
        ax.plot(range(len(test_loss_values)), test_loss_values, label="Test")

        ax.legend()
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Loss")

        plt.savefig(f"assets/{title}.png", dpi=80)
        plt.close('all')

        self.parameters_values = current_parameters_values