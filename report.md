# <center>Report</center>
## <center>Multilayer Perceptron </center> <br />
#### <center>March, 2020 </center> <br />
#### <center>Michał Dyczko</center>

## Table of Contents <br />

1. [Documentation and User Interface](#documentation_and_user_interface).
1. [Model tuning on provided datasets](#tuning):
    1. [Binary Classification](#bc):
        * Data simple:
            * Activation function: sigmoid, tanh, Leaky ReLU;
            * Number of layers: 0, 1, 2, 3, 4;
            * Loss function: binary crossentropy, logistic loss.
    1. [Multiple Classification](#mc):
        * Three Gauss:
            * Activation function: sigmoid, tanh, Leaky ReLU;
            * Number of layers: 0, 1, 2, 3, 4;
            * Loss function: binary crossentropy, logistic loss.
    1. [Regression](#r):
        * Activation:
            * Activation function: sigmoid, tanh, Leaky ReLU;
            * Number of layers: 0, 1, 2, 3, 4;
            * Loss function: binary crossentropy, logistic loss,
1. [Kaggle Digit Recognizer](#kaggle).

##  Documentation and User Interface <a name="documentation_and_user_interface"></a><br />

### User Interface<br />

Main components of API used to train model are methods:
* add_layer(*)
* train(*)
* predict(*)

### add_layer:
<div style="text-align:justify">
First step that user ought to do is to define architecture of MLP by adding layers with information of number of neurons in each and optionally by providing activation function (default is ReLU). It is done by sequentially executing method add_layer. Details can be found in docstrings in mlp.py file.

Note: Instead of that one can assign dict in form of {argument: value} to mlp.architecture. Then every argument should be filled.
</div>

**Arguments:**
* input_dim (int): Number of neurons in a layer. Should be equal to output_dim of previous layer.
* output_dim (int, optional): Number of values returned by a layer. Defaults to 1.
* activation (str, optional): Activation function of a layer.
                Supported values: "relu", "sigmoid", "softmax" (on the last layer), "linear", "leaky_relu", "tanh". Defaults to "relu".

**Example of use:**

```python
mlp = MLP()
mlp.add_layer(3, activation="leaky_relu")
mlp.add_layer(10, activation="tanh")
mlp.add_layer(5)
```
<br />
<br />

### train:
<div style="text-align:justify">
Next step is to train model providing such arguments as: X (training features), Y (output) and training customizations as presence of bias, batch_size, epochs, early_stopping etc. It can be done independently by defining each argument or in almost completely automatic way by providing only X, Y and mode. The chosen mode will result in automatic data preprocessing, architecture adjustments and setting output activation function. Details can be found in docstrings in mlp.py file.
</div>

**Arguments:**

* X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in model training and optionally for validation purposes.
* Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in model training and optionally for validation purposes.
* bias (bool, optional): Presence of biases in layers. Defaults to True.
* batch_size (int, optional): Size of training sample in single iteration. Defaults to 32.
* iterations (int, optional): Maximum value of iterations. Defaults to None.
* learning_rate (float, optional): Learning rate. Defaults to 0.01.
* momentum_rate (float, optional): Momentum rate. Defaults to 0.9.
* mode (string, optional): Mode used in training.
                Supported values: "regression", "classification", "binary_classification", "multiple_classification".
                It results in loss attribute override, architecture adjustments and data preprocessing.
                Defaults to None.
* loss (str, optional): Loss function.
                Supported values: "binary_crossentropy", "categorical_crossentropy", "mean_squared_error".
                Defaults to "binary_crossentropy".
* epochs (int, optional): Number of epochs of training process. Defaults to 100.
* verbose (bool, optional): Training process verbosity. Defaults to False.
* seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.
* early_stopping (bool, optional): Defines if early stopping technique is used.. Defaults to False.
* patience (int, optional): Early stopping patience parameter.
                Represents a maximum number of epochs that model 'waits' for validation loss improvement.
                Defaults to 3.
* validation_split (float, optional): Training size ratio. Defaults to None.

**Returns**

* array: Outputs predicted by MLP.

**Example of use:**
```python
mlp = MLP()
mlp.add_layer(*)
...

mlp.train(
    X=X_train,
    Y=Y_train,
    bias=True,
    loss="binary_crossentropy",
    learning_rate=0.01,
    early_stopping=True,
    validation_split=0.65
)
# or it can be for instance: mlp.train(X, Y, mode="binary_classification")
```
### predict:
<div style="text-align:justify">
The last step is to predict values providing such arguments as X (test features) and Y (test output). Y is provided to calculate accuracy and loss on test set. Details can be found in docstrings in mlp.py file
</div>

**Arguments:**

* X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
                Used in outputs generation.
* Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
                Used in loss and accuracy calculation.
            verbose (bool, optional): Verbosity of a prediction process. Defaults to True.

**Example of use:**
```python
mlp = MLP()
mlp.add_layer(*)
...
mlp.train(*)
mlp.predict(X_test, Y_test)
```

### Documentation

<div style="text-align: justify">
MLP is trained by backward propagation algorithm and Gradient Descent (with momentum). Training process can be segmented into steps:

1. Load mode (optionally) and assign attributes: <code>load_attributes</code>,
1. Init weights and biases: <code>init_layers</code>,
1. Init cache and history of training: <code>init_history_and_cache</code>,
1. Split dataset into training and validation <code>dataset_split</code>. Split into batches <code>split_training_set</code>.
1. For each epoch:
    1. For each batch:
        1. Propagate forward. <code>forward_propagation</code>
        1. Process backward propagation and cache gradient and velocity. <code>backward_propagation</code>
        1. Update weights and biases.<code>update</code>
    1. Save loss and accuracy in history.<code>update_history_and_cache</code>
    1. Stop early if loss on validaiton hasn't decreased in previous <code>patience</code> epochs.
1. Clear cache.

__Below are presented docstrings for these methods__:

```python
def load_attributes(self, X, Y, bias, batch_size, iterations, learning_rate, momentum_rate, mode, loss, seed):
    """Loads model attributes.

    Args:
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Used in model training and optionally for validation purposes.
        Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            Used in model training and optionally for validation purposes.
        bias (bool, optional): Presence of biases in layers. Defaults to True.
        batch_size (int, optional): Size of training sample in single iteration. Defaults to 32.
        iterations (int, optional): Maximum value of iterations. Defaults to None.
        learning_rate (float, optional): Learning rate. Defaults to 0.01.
        momentum_rate (float, optional): Momentum rate. Defaults to 0.9.
        mode (string, optional): Mode used in training.
            Supported values: "regression", "classification", "binary_classification", "multiple_classification".
            It results in loss attribute override, architecture adjustments and data preprocessing.
            Defaults to None.
        loss (str, optional): Loss function.
            Supported values: "binary_crossentropy", "categorical_crossentropy", "mean_squared_error".
            Defaults to "binary_crossentropy".
        epochs (int, optional): Number of epochs of training process. Defaults to 100.
        verbose (bool, optional): Training process verbosity. Defaults to False.
        seed (int, optional):  Represents a seed used in pseudorandom numbers generation.. Defaults to 2.

    Returns:
        (
            array: Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes.
            array: Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
        )
    """

def init_layers(self, seed):
    """Initialize weights and biases of layers in Multilayer Perceptron.

    Args:
        seed (int): Represents a seed used in pseudorandom numbers generation.

    Returns:
        dict: Weights and biases of layers in form of a dict with keys like "W{layer_id}" and "b{layer_id}", where layer_id is numbered from 1.
    """

    architecture = self.architecture
    bias = self.bias
    np.random.seed(seed)
    parameters_values = {}

    for layer_id, layer in enumerate(architecture, 1):
        layer_input_size = layer["input_dim"]
        layer_output_size = layer["output_dim"]
        parameters_values[f"W{layer_id}"] = np.random.randn(layer_output_size, layer_input_size) * 0.1
        parameters_values[f"b{layer_id}"] = np.random.randn(layer_output_size, 1) * 0.1 if bias else np.zeros((layer_output_size, 1))

    return parameters_values

def init_history_and_cache(self):
    """Initialize history and cache attributes.
    """

def dataset_split(self, X_prepared, Y_prepared, validation_split):
    """Performs dataset split based on given ratio.

    Args:
        X_prepared (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples, transformed and convenient for training purposes.
        Y_prepared (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples, transformed and convenient for training purposes.
        validation_split (float): Percentage of training set.

    Returns:
        (
            array: X_prepared subset for training purposes.
            array: Y_prepared subset for training purposes.
            array: X_prepared subset for validation purposes.
            array: Y_prepared subset for validation purposes.
        )
    """

def split_training_set(self, X, Y):
    """Splits training set into batches.

    Args:
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Used in model training and optionally for validation purposes.
        Y (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            Used in model training and optionally for validation purposes.

    Returns:(
        list: Batches of X.
        list: Batches of Y.
    )
    """

def forward_propagation(self, X):
    """Performs forward propagation from the input layer to the output layer.

    Args:
        X (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.

    Returns:
        array: Outputs generated by the Multilayer Perceptron.
    """

    architecture = self.architecture
    parameters_values = self.parameters_values
    A_curr = X

    for idx, layer in enumerate(architecture):
        layer_idx = idx + 1
        A_prev = A_curr
        activation_curr = layer["activation"]
        W_curr = parameters_values[f"W{layer_idx}"]
        b_curr = parameters_values[f"b{layer_idx}"]
        A_curr, Z_curr = self.one_step_forward_propagation(A_prev, W_curr, b_curr, activation=activation_curr)
        self.cache[f"A{idx}"] = A_prev
        self.cache[f"Z{layer_idx}"] = Z_curr

    return A_curr

def backward_propagation(self, Y_hat, Y):
    """Performs backward propagation from the output layer to the input layer.

    Args:
        Y_hat (array): Outputs generated by the Multilayer Perceptron.
        Y (array): Real outputs.

    Returns:
    (
        dict: {
            "dW{layer_id}": Correction of weights of the {layer_id} layer based on gradient.
            "db{layer_id}": Correction of bias of the {layer_id} layer based on gradient.
        }
        dict: {
            "dW{layer_id}": Correction of weights of the {layer_id} layer in a previous iteration.
            "db{layer_id}": Correction of bias of the {layer_id} layer in a previous iteration.
        }
    )
    """

def update(self, gradient_values_curr, learning_rate, velocity_values_prev, momentum_rate):
    """Performs weights and biases optimization.

    Args:
        gradient_values_curr (array): dict: {
                "dW{layer_id}": Correction of weights of the {layer_id} layer based on gradient.
                "db{layer_id}": Correction of bias of the {layer_id} layer based on gradient.
            }
        learning_rate (float): Learning rate.
        velocity_values_prev (array): dict: {
                "dW{layer_id}": Correction of weights of the {layer_id} layer in a previous iteration.
                "db{layer_id}": Correction of bias of the {layer_id} layer in a previous iteration.
            }
        momentum_rate (float): Momentum rate.

    Returns:
        array: Optimized weights and biases.
    """

def update_history_and_cache(self, X_train_prepared, Y_train_prepared, X_val_prepared, Y_val_prepared, epoch):
    """Stores informations generated in each epoch such like:
        values of parameters
        loss obtained on training set
        accuracy obtained on training set
        loss obtained on validation set
        accuracy obtained on validation set

    Args:
        X_train_prepared (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Used in model training.
        Y_train_prepared (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            Used in model training.
        X_val_prepared (array): Features in form of an array of shape (n,m), where n represents number of features and m is a number of examples.
            Used in model validation.
        Y_val_prepared (array): Outputs in form of an array of shape (n,m), where n represents number of estimated values and m is a number of examples.
            Used in model validation.
        epoch (int): Number of epoch.

    Returns:
        (
            float: Value of loss on training set.
            float: Value of accuracy on training set.
            float: Value of loss on validation set.
            float: Value of accuracy on validation set.
        )
    """
```

</div>

<style>
td, th{
    border:1px solid #000;
    padding: 8px;
}
</style>

## Model tuning on provided datasets <a name="tuning"></a>:

### Binary Classification <a name="bc"></a>:

Comparison among models will be done customizing the mode "binary_classification" in assumption of early stopping and 0.7 trainig/validation ratio. Training will be processed on 1000 examples dataset, just as the testing. Every hidden layer will have 5 neurons. Parameters set as "default" for binary classification will be:

* bias: True,
* batch_size: 32,
* learning_rate: 0.01,
* momentum_rate: 0.9,
* mode: "binary_classification",
* epochs: 500,
* verbose: True,
* early_stopping: True,
* patience: 20,
* validation_split: 0.7

There will be tested impact of the following factors on test classification accuracy:

* Activation function: sigmoid, tanh, Leaky ReLU,
* Number of hidden layers: 0, 1, 2, 3, 4,
* Loss function: binary crossentropy, RMSE

#### Data simple

<div>
<p>
<b>0 hidden layers, binary crossentropy</b>

There is no layer in which we can change activation function. After 500 epochs network achieves surprisingly high accuracy at level of 99.2%.
</p>
<img src="assets/BC_0_BC_sigmoid.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9920000000</td>
<td>0.0815770222</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>0 hidden layers, RMSE</b>

There is no layer in which we can change activation function. After 500 epochs network achieves high accuracy at level of 99.3%.
</p>
<img src="assets/BC_0_RMSE_sigmoid.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9930000000</td>
<td>0.1488840089</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, binary crossentropy, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 99.7%.
</p>
<img src="assets/BC_1_BC_sigmoid.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9970000000</td>
<td>0.0380261128</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, binary crossentropy, tanh</b>

After 500 epochs network achieves high accuracy at level of 99.8%.
</p>
<img src="assets/BC_1_BC_tanh.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9980000000</td>
<td>0.0188319655</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, binary crossentropy, Leaky ReLU</b>

After 500 epochs network achieves high accuracy at level of 99.6%.
</p>

<img src="assets/BC_1_BC_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9960000000</td>
<td>0.0090645577</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 99.5%.
</p>

<img src="assets/BC_1_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9950000000</td>
<td>0.0956808192</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, tanh</b>

After 500 epochs network achieves high accuracy at level of 99.7%.
</p>

<img src="assets/BC_1_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9970000000</td>
<td>0.0634099766</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, Leaky ReLU</b>

After 500 epochs network achieves high accuracy at level of 99.8%.
</p>

<img src="assets/BC_1_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9980000000</td>
<td>0.0637475508</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, binary crossentropy, sigmoid</b>

MLP was stopped because loss value on validation set didn't decreased during first 20 epochs.

</p>

<img src="assets/BC_2_BC_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.4920000000</td>
<td>0.6956061546</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, binary crossentropy, tanh</b>

After 500 epochs network achieves high accuracy at level of 99.5%.

</p>

<img src="assets/BC_2_BC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9950000000</td>
<td>0.0108392713</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, binary crossentropy, Leaky ReLU</b>

After 500 epochs network achieves high accuracy at level of 99.5%.

</p>

<img src="assets/BC_2_BC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9950000000</td>
<td>0.0107509148</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, binary crossentropy, mixed (tanh, Leaky ReLU)</b>

After 500 epochs network achieves high accuracy at level of 99.7%.

</p>

<img src="assets/BC_2_BC_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9970000000</td>
<td>0.0135222090</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, sigmoid</b>

MLP was stopped because loss value on validation set didn't decreased during first 20 epochs.

</p>

<img src="assets/BC_2_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.4920000000</td>
<td>0.5009571773</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, tanh</b>

After 500 epochs network achieves high accuracy at level of 99.6%.
</p>

<img src="assets/BC_2_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9960000000</td>
<td>0.0571118284</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, Leaky ReLU</b>

After 500 epochs network achieves high accuracy at level of 99.3%.
</p>

<img src="assets/BC_2_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9930000000</td>
<td>0.0616459991</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, mixed (tanh, Leaky ReLU)</b>

MLP was stopped because loss value on validation set didn't decreased during first 20 epochs.
</p>

<img src="assets/BC_2_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.4920000000</td>
<td>0.5009342815</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, BC, tanh</b>

This is the only 3-layered architecture in which loss decreased during 20 epochs. After 500 epochs network achieved high accuracy at level of 99.6%.
</p>

<img src="assets/BC_3_BC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9960000000</td>
<td>0.0138732751</td>
</tbody>
</table>
</div>
<hr />

In 4-layered MLP only architecture - binary crossentropy, tanh - decreased, but it was so small, that accuracy was approximately 50%.

### Summary

Only simple architectures occured to be proper in this task. In two-layer MLP sigmoid failed to provide good accuracy quickly. Binary crossentropy and RMSE lead to similiar results.
Best architecture was 1-layered MLP with tanh activation function and binary crossentropy loss function. The remedy for quicker loss decrease in complex architectures would be batch size decrease, learning rate increase or patience increase.

<br />
<br />
<br />
<br />
<br />

### Multiple Classification <a name="mc"></a>:

#### Three Gauss

Comparison among models will be done customizing the mode "multiple_classification" in assumption of early stopping and 0.7 trainig/validation ratio. Training will be processed on 1000 examples dataset, just as the testing. Every hidden layer has 10 neurons. Parameters set as "default" for multiple classification will be:

* bias: True,
* batch_size: 32,
* learning_rate: 0.01,
* momentum_rate: 0.9,
* mode: "multiple_classification",
* epochs: 500,
* verbose: True,
* early_stopping: True,
* patience: 20,
* validation_split: 0.7

There will be tested impact of the following factors on test classification accuracy:

* Activation function: sigmoid, tanh, Leaky ReLU,
* Number of hidden layers: 0, 1, 2, 3, 4,
* Loss function: binary crossentropy, RMSE


<div>
<p>
<b>0 hidden layers, categorical crossentropy</b>

There is no layer in which we can change activation function. After 500 epochs network achieves surprisingly high accuracy at level of 92.6%.
</p>
<img src="assets/MC_0_CC_softmax.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9260000000</td>
<td>0.2164890015</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>0 hidden layers, RMSE</b>

There is no layer in which we can change activation function. After 500 epochs network achieves high accuracy at level of 92.7%.
</p>
<img src="assets/MC_0_RMSE_softmax.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9270000000</td>
<td>0.1927711907</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, categorical crossentropy, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 92.7%.
</p>
<img src="assets/MC_1_CC_sigmoid.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9273333333</td>
<td>0.1876417754</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, categorical crossentropy, tanh</b>

After 500 epochs network achieves high accuracy at level of 93.4%.
</p>
<img src="assets/MC_1_CC_tanh.png" />
<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9343333333</td>
<td>0.1644553777</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, categorical crossentropy, Leaky ReLU</b>

After 500 epochs network achieves high accuracy at level of 93.3%.
</p>

<img src="assets/MC_1_CC_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9330000000</td>
<td>0.1858061699</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 92.7%.
</p>

<img src="assets/MC_1_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9273333333</td>
<td>0.1851437890</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, tanh</b>

After 500 epochs network achieves high accuracy at level of 93.4%.
</p>

<img src="assets/MC_1_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9343333333</td>
<td>0.1770214915</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, Leaky ReLU</b>

After 35 epochs network stops at high accuracy at level of 92.7%.
</p>

<img src="assets/MC_1_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9270000000</td>
<td>0.1923094677</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, categorical crossentropy, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 92.9%.

</p>

<img src="assets/MC_2_CC_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9293333333</td>
<td>0.1795236366</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, categorical crossentropy, tanh</b>

After 361 epochs network stops at high accuracy at level of 93.7%.

</p>

<img src="assets/MC_2_CC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9373333333</td>
<td>0.1591955233</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, categorical crossentropy, Leaky ReLU</b>

After 224 epochs network stops at high accuracy at level of 93.6%.

</p>

<img src="assets/MC_2_CC_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9363333333</td>
<td>0.1664920712</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, categorical crossentropy, mixed (tanh, Leaky ReLU)</b>

After 248 epochs network stops at high accuracy at level of 93.5%.

</p>

<img src="assets/MC_2_CC_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9353333333</td>
<td>0.1652953682</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, sigmoid</b>

After 123 epochs network stops at high accuracy at level of 92.9%.

</p>

<img src="assets/BC_2_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9296666667</td>
<td>0.1903100738</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, tanh</b>

After 500 epochs network achieves high accuracy at level of 93.6%.
</p>

<img src="assets/MC_2_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9366666667</td>
<td>0.1743709671</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, Leaky ReLU</b>

After 251 epochs network achieves high accuracy at level of 93.6%.
</p>

<img src="assets/MC_2_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9363333333</td>
<td>0.1767496877</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, mixed (tanh, Leaky ReLU)</b>

After 248 epochs network achieves high accuracy at level of 93.5%.
</p>

<img src="assets/MC_2_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9353333333</td>
<td>0.1767311365</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, CC, sigmoid</b>

After 500 epochs network achieves high accuracy at level of 92.9%. It starts decreasing drastically in about 250 epoch.

</p>

<img src="assets/MC_3_CC_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9293333333</td>
<td>0.1815716501</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, CC, tanh</b>

After 500 epochs network achieves high accuracy at level of 93.6%.

</p>

<img src="assets/MC_3_CC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9363333333</td>
<td>0.1604294241</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, CC, Leaky ReLU</b>

After 243 epochs network stops at high accuracy at level of 93.6%.

</p>

<img src="assets/MC_3_CC_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9363333333</td>
<td>0.1630963682</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, CC, mixed (tanh, Leaky ReLU, tanh)</b>

After 219 epochs network stops at high accuracy at level of 93.7%.

</p>

<img src="assets/MC_3_CC_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9370000000</td>
<td>0.1611050096</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, sigmoid</b>

After 500 epochs network stops at high accuracy at level of 92.9%. It is important to notice that loss starts decresing in about 250 epoch.

</p>

<img src="assets/MC_3_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9293333333</td>
<td>0.1820728877</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, tanh</b>

After 242 epochs network stops at high accuracy at level of 93.7%.
</p>

<img src="assets/MC_3_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9370000000</td>
<td>0.1749682226</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, Leaky ReLU</b>

After 42 epochs network stops at high accuracy at level of 92.7%.
</p>

<img src="assets/MC_3_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9276666667</td>
<td>0.1833867291</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, mixed (tanh, Leaky ReLU, tanh)</b>

After 42 epochs network stops at high accuracy at level of 93.7%.
</p>

<img src="assets/MC_3_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9370000000</td>
<td>0.1750306445</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, CC, tanh</b>

After 179 epochs network stops at high accuracy at level of 93.5%.
</p>

<img src="assets/MC_4_CC_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9356666667</td>
<td>0.1617630277</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, CC, Leaky ReLU</b>

After 213 epochs network stops at high accuracy at level of 93.4%.
</p>

<img src="assets/MC_4_CC_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9346666667</td>
<td>0.1662763831</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, CC, mixed (tanh, Leak ReLU, tanh, Leaky ReLU)</b>

After 131 epochs network stops at high accuracy at level of 93.5%.
</p>

<img src="assets/MC_4_CC_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9356666667</td>
<td>0.1657044331</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, tanh</b>

After 198 epochs network stops at high accuracy at level of 93.6%.
</p>

<img src="assets/MC_4_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9363333333</td>
<td>0.1753932668</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, Leaky ReLU</b>

After 168 epochs network stops at high accuracy at level of 93.4%.
</p>

<img src="assets/MC_4_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9346666667</td>
<td>0.1786677874</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, mixed (tanh, Leaky ReLU, tanh, Leaky ReLU)</b>

After 80 epochs network stops at high accuracy at level of 93.4%.
</p>

<img src="assets/MC_4_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.9340000000</td>
<td>0.1797546933</td>
</tbody>
</table>
</div>
<hr />
.

### Summary

Adding more layers than 2 didn't make an improvement in accuracy. Sigmoid was not a good choice in this problem. It didnt' lead to validation loss decrease. There were no big difference between RMSE and categorical crossentropy observed. The best accuracy (0.9373333333) were achieved in the following architecture: 2 layers with tanh activation and categorical crossentropy loss function.


### Regression <a name="r"></a>:
#### Activation:
Comparison among models will be done customizing the mode "regression" in assumption of early stopping and 0.7 trainig/validation ratio. Training will be processed on 1000 examples dataset, just as the testing. Every hidden layer has 25 neurons. Parameters set as "default" for multiple classification will be:

* bias: True,
* batch_size: 8,
* learning_rate: 0.0005,
* momentum_rate: 0.8,
* mode: "regression",
* epochs: 1000,
* verbose: True,
* early_stopping: True,
* patience: 50,
* validation_split: 0.7

There will be tested impact of the following factors on test classification accuracy:

* Activation function: sigmoid, tanh, Leaky ReLU,
* Number of hidden layers: 0, 1, 2, 3, 4,
* Loss function: MSE, RMSE


<div>
<p>
<b>0 hidden layers, MSE</b>

There is no layer in which we can change activation function. After 154 epochs network achieves loss at level of 0.0341986046.
</p>
<img src="assets/R_0_MSE_linear.png" />
<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>48.4531405202</td>
<td>0.0341986046</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>0 hidden layers, RMSE</b>

There is no layer in which we can change activation function. After 56 epochs network achieves loss at level of 0.1995026152.
</p>
<img src="assets/R_0_RMSE_linear.png" />
<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>52.2716646820</td>
<td>0.1995026152</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, MSE, sigmoid</b>

After 1000 epochs network achieves loss at level of 0.0835811747. Despite early stopping one can notice overfitting of the MLP about 400th epoch.
</p>
<img src="assets/R_1_MSE_sigmoid.png" />
<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>75.7481400654</td>
<td>0.0835811747</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, MSE, tanh</b>

After 1000 epochs network achieves loss at level of 0.0108810713. There is a pick about 300th epoch in test loss, but then it noticeably decresases.
</p>
<img src="assets/R_1_MSE_tanh.png" />
<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>27.3308769624</td>
<td>0.0108810713</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, MSE, Leaky ReLU</b>

After 1000 epochs network achieves loss at level of 0.0348706409. One can notice overfitting in the beginning of the plot below.
</p>

<img src="assets/R_1_MSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>48.9269003196</td>
<td>0.0348706409</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, sigmoid</b>

After 318 epochs network achieves loss at level of 0.1140995985. We can see that model parameters are not stable.

</p>

<img src="assets/R_1_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>29.8952269229</td>
<td>0.1140995985</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, tanh</b>

After 338 epochs network achieves very precise accuracy of 0.6858289129.
</p>

<img src="assets/MC_1_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.6858289129</td>
<td>0.0026175685</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>1 hidden layer, RMSE, Leaky ReLU</b>

After 76 epochs network stops at RMSE of predictions at level 48.2321177159.
</p>

<img src="assets/R_1_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>48.2321177159</td>
<td>0.1840850809</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, MSE, sigmoid</b>

After 1000 epochs network achieves RMSE of predictions at 76.2446733513. It is the result of overfitting visible on the plot below.

</p>

<img src="assets/R_2_MSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>76.2446733513</td>
<td>0.0846805246</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, MSE, tanh</b>

After 1000 epochs network achieves RMSE 10.3900524386 on test set. After about 150 epoch training leads to test set loss decrease.

</p>

<img src="assets/R_2_MSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>10.3900524386</td>
<td>0.0015725338</td>
</tbody>
</table>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, MSE, Leaky ReLU</b>

After 1000 epochs network achieves RMSE 3.5865899878 on test set.

</p>

<img src="assets/R_2_MSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>3.5865899878</td>
<td>0.0001873821</td>
</tbody>
</table>
</table>
</div>
<hr />

<div>
<p>
<b>2 hidden layer, MSE, mixed (tanh, Leaky ReLU)</b>

After 1000 epochs network achieves RMSE 11.3847712495 on test set. After about 150 epoch training leads to test set loss decrease.

</p>

<img src="assets/R_2_MSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>11.3847712495</td>
<td>0.0018880485</td>
</tbody>
</table>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, sigmoid</b>

After 440 epochs network achieves RMSE 25.3558274335 on test set. After about 200 epoch training leads to test set loss decrease. Loss in not stable in the end of training.

</p>

<img src="assets/R_2_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>25.3558274335</td>
<td>0.0967743024</td>
</tbody>
</table>
</table>
</div>
<hr />



<div>
<p>
<b>2 hidden layer, RMSE, tanh</b>

After 196 epochs network achieves very good RMSE 0.4643522233 on test set.
</p>

<img src="assets/R_2_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>0.4643522233</td>
<td>0.0017722696</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, Leaky ReLU</b>

After 109 epochs network achieves good RMSE 4.4598256662 on test set.
</p>

<img src="assets/R_2_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>4.4598256662</td>
<td>0.0170215907</td>
</tbody>
</table>
</div>
<hr />


<div>
<p>
<b>2 hidden layer, RMSE, mixed (tanh, Leaky ReLU)</b>

After 248 epochs network achieves RMSE 13.8332727364 on test set.
</p>

<img src="assets/R_2_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>13.8332727364</td>
<td>0.0527967514</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, MSE, sigmoid</b>

After 400 epoch network is overfitting data. It ends with RMSE on test set 74.5285335680.

</p>

<img src="assets/R_3_MSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>74.5285335680</td>
<td>0.0809113926</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, MSE, tanh</b>

After 1000 epochs network achieves RMSE 2.5269851413 on test set.

</p>

<img src="assets/R_3_MSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>2.5269851413</td>
<td>0.0000930186</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, MSE, Leaky ReLU</b>

After 1000 epochs network stops with RMSE on test set equal to 10.4747653723.

</p>

<img src="assets/R_3_MSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>10.4747653723</td>
<td>0.0015982810</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, MSE, mixed (tanh, Leaky ReLU, tanh)</b>

After 100 epochs network stops with RMSE on test set equal to 40.5402570622.

</p>

<img src="assets/R_3_MSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>40.5402570622</td>
<td>0.0239407372</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, sigmoid</b>

This architecture fails in prediction after 54 epochs.

</p>

<img src="assets/R_3_RMSE_sigmoid.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>123.7629366975</td>
<td>0.4723597322</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, tanh</b>

After 200 epochs network stops with very good RMSE on test set equal to 1.1555117183.
</p>

<img src="assets/R_3_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>1.1555117183</td>
<td>0.0044101831</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, Leaky ReLU</b>

After 118 epochs network stops at high accuracy at level of 92.7%.
</p>

<img src="assets/R_3_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>9.7303082288</td>
<td>0.0371371746</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>3 hidden layer, RMSE, mixed (tanh, Leaky ReLU, tanh)</b>

After 185 epochs network achieved RMSE at level 14.3712746002.
</p>

<img src="assets/R_3_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>14.3712746002</td>
<td>0.0548501159</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, MSE, tanh</b>

After 1000 epochs network stops with very good RMSE 1.4617112673 on test set.
</p>

<img src="assets/R_4_MSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>1.4617112673</td>
<td>0.0000311234</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, MSE, Leaky ReLU</b>

After 1000 epochs network ends training with RMSE 12.8146335622 on test set.
</p>

<img src="assets/R_4_MSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>12.8146335622</td>
<td>0.0023920867</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, MSE, mixed (tanh, Leak ReLU, tanh, tanh)</b>

After 1000 epochs network stops with RMSE 31.9155300184 on test set.
</p>

<img src="assets/R_4_MSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>31.9155300184</td>
<td>0.0148377704</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, tanh</b>

After 83 epochs network end with very good RMSE 1.1300172070 on test set.
</p>

<img src="assets/R_4_RMSE_tanh.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>1.1300172070 (RMSE)</td>
<td>0.0043128794</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, Leaky ReLU</b>

After 104 epochs network stops with RMSE 17.1458041032 on test set. After significant loss decrease MLP didn't make improvements.
</p>

<img src="assets/R_4_RMSE_leaky_relu.png" />

<table>
<thead>
<tr>
<th>Accuracy (RMSE)</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>17.1458041032</td>
<td>0.0654395221</td>
</tbody>
</table>
</div>
<hr />

<div>
<p>
<b>4 hidden layer, RMSE, mixed (tanh, Leaky ReLU, tanh, Leaky ReLU)</b>

After 111 epochs network stops training with RMSE 4.4123061666 on test set.
</p>

<img src="assets/R_4_RMSE_mixed.png" />

<table>
<thead>
<tr>
<th>Accuracy</th>
<th>Loss</th>
</tr>
</thead>
<tbody>
<td>4.4123061666</td>
<td>0.0168402255</td>
</tbody>
</table>
</div>
<hr />
.

### Summary

Adding more layers than 1 didn't make an improvement in accuracy. Sigmoid was not a good choice in this problem, it lead to long training and little loss improvements. RMSE resulted in much faster loss decrease. The best RMSE of predictions on test set were achieved in the following architecture: 1 layer with tanh activation and RMSE loss function.

## Kaggle Digit Recognizer <a name="kaggle"></a>

Kaggle Digit Recognizer is a competition in which a goal is to recognize handwritten digits and classify them. The images of such digits are provided ina form of dataset with 785 columns. First is a label, which is simply a real digit. Other 784 columns are pixel values which is provided as training for a model. There is also test set that differs from training set only in the lack of label column.

The main task in that competition is to use machine learning model to fit label to test data. In this case, implementation of MLP from this project will be used.

### Step by step solution

There were tested many architectures with parameters from the following options:

* Activation function: sigmoid, tanh, Leaky ReLU;
* Number of layers: 0, 1, 2, 3, 4;
* Loss function: binary crossentropy, logistic loss.

To prevent overfitting early stopping were used with patience parameter equal to 3.

As were tested Leaky ReLU lead to calculation problems. As the result MLP with Leaky ReLU on every layer couldn't be used. Setting tanh or sigmoid in first layer helped with that. RMSE provided better results than categorical crossentropy with Leaky ReLU, but it didn't outperform MLP (with various number of layers) with tanh or sigmoid only and categorical crossentropy. Difference were about 95 to 90 percent of accuracy on validation set. That was a reason to consider only tanh and sigmoid from that point.

It is claimed that 2 hidden layer is enough to model any arbitrary function there were considered only 1 and 2-layer networks.

Following a rule of a thumb to choose number of neurons between input and ouput dimension and an opinion that (Sarle 1995) that there seems to be no upper limit on hidden units if early stopping is used, then total hidden units were set to 784 (equal to input dimension).

**First architecture**:

```python

mlp = MLP()
mlp.add_layer(784, activation="sigmoid")
mlp.add_layer(784, activation="sigmoid")


mlp.train(
    X=X_train,
    Y=Y_train,
    bias=True,
    batch_size=64,
    learning_rate=0.005,
    momentum_rate=0.8,
    mode="multiple_classification",
    loss="categoric_crossentropy",
    epochs=100,
    verbose=True,
    early_stopping=True,
    patience=3,
    validation_split=0.7
)

```

The log from a training process is as following

```python

Epoch: 00001  - train loss: 0.3816658205 - train accuracy: 0.8886356679 - val loss: 0.4033971945 - val accuracy: 0.8812792636
Epoch: 00002  - train loss: 0.3018217059 - train accuracy: 0.9129222082 - val loss: 0.3303353745 - val accuracy: 0.9023886993
Epoch: 00003  - train loss: 0.2607535280 - train accuracy: 0.9265281132 - val loss: 0.2950507046 - val accuracy: 0.9127846996
Epoch: 00004  - train loss: 0.2384870789 - train accuracy: 0.9318684309 - val loss: 0.2752312219 - val accuracy: 0.9178636616
Epoch: 00005  - train loss: 0.2132047146 - train accuracy: 0.9408143134 - val loss: 0.2542545645 - val accuracy: 0.9252440283
Epoch: 00006  - train loss: 0.1974691748 - train accuracy: 0.9455083506 - val loss: 0.2410667006 - val accuracy: 0.9276247917
Epoch: 00007  - train loss: 0.1847696176 - train accuracy: 0.9500663288 - val loss: 0.2326670788 - val accuracy: 0.9296087612
Epoch: 00008  - train loss: 0.1751739729 - train accuracy: 0.9515969931 - val loss: 0.2262833537 - val accuracy: 0.9314340132
Epoch: 00009  - train loss: 0.1661555272 - train accuracy: 0.9541140855 - val loss: 0.2182312746 - val accuracy: 0.9363542576
Epoch: 00010  - train loss: 0.1546603449 - train accuracy: 0.9581618422 - val loss: 0.2104856086 - val accuracy: 0.9348464408
Epoch: 00011  - train loss: 0.1469987088 - train accuracy: 0.9602367427 - val loss: 0.2033254316 - val accuracy: 0.9391318149
Epoch: 00012  - train loss: 0.1397356550 - train accuracy: 0.9619034661 - val loss: 0.1979574052 - val accuracy: 0.9415125784
Epoch: 00013  - train loss: 0.1349316519 - train accuracy: 0.9631279976 - val loss: 0.1936584956 - val accuracy: 0.9414332196
Epoch: 00014  - train loss: 0.1271426806 - train accuracy: 0.9654410014 - val loss: 0.1866997959 - val accuracy: 0.9449250060
Epoch: 00015  - train loss: 0.1211944247 - train accuracy: 0.9684683152 - val loss: 0.1844093328 - val accuracy: 0.9446869296
Epoch: 00016  - train loss: 0.1189372825 - train accuracy: 0.9687744481 - val loss: 0.1819706322 - val accuracy: 0.9449250060
Epoch: 00017  - train loss: 0.1093299697 - train accuracy: 0.9711894962 - val loss: 0.1796101400 - val accuracy: 0.9456392350
Epoch: 00018  - train loss: 0.1087093690 - train accuracy: 0.9713255553 - val loss: 0.1753378821 - val accuracy: 0.9452424411
Epoch: 00019  - train loss: 0.1016275668 - train accuracy: 0.9734004558 - val loss: 0.1741788743 - val accuracy: 0.9468296167
Epoch: 00020  - train loss: 0.0967089999 - train accuracy: 0.9747610463 - val loss: 0.1687119018 - val accuracy: 0.9486548687
Epoch: 00021  - train loss: 0.0924930571 - train accuracy: 0.9766998878 - val loss: 0.1671628177 - val accuracy: 0.9500039679
Epoch: 00022  - train loss: 0.0910209104 - train accuracy: 0.9769039763 - val loss: 0.1652116411 - val accuracy: 0.9499246092
Epoch: 00023  - train loss: 0.0862560649 - train accuracy: 0.9781965373 - val loss: 0.1629357353 - val accuracy: 0.9509562733
Epoch: 00024  - train loss: 0.0843588710 - train accuracy: 0.9796591721 - val loss: 0.1591797986 - val accuracy: 0.9517498611
Epoch: 00025  - train loss: 0.0770901319 - train accuracy: 0.9814279397 - val loss: 0.1549646691 - val accuracy: 0.9519879375
Epoch: 00026  - train loss: 0.0761801920 - train accuracy: 0.9818361169 - val loss: 0.1591051319 - val accuracy: 0.9509562733
Epoch: 00027  - train loss: 0.0728938614 - train accuracy: 0.9829926188 - val loss: 0.1558982020 - val accuracy: 0.9539719070
Epoch: 00028  - train loss: 0.0690853814 - train accuracy: 0.9842511650 - val loss: 0.1549076134 - val accuracy: 0.9535751131
Epoch: 00029  - train loss: 0.0678746348 - train accuracy: 0.9839450321 - val loss: 0.1525726848 - val accuracy: 0.9530196016
Epoch: 00030  - train loss: 0.0628765143 - train accuracy: 0.9860879622 - val loss: 0.1523767969 - val accuracy: 0.9519879375
Epoch: 00031  - train loss: 0.0623303364 - train accuracy: 0.9859178884 - val loss: 0.1508285696 - val accuracy: 0.9535751131
Epoch: 00032  - train loss: 0.0590017700 - train accuracy: 0.9862920508 - val loss: 0.1503673456 - val accuracy: 0.9545274185
Epoch: 00033  - train loss: 0.0557432762 - train accuracy: 0.9883329365 - val loss: 0.1492924960 - val accuracy: 0.9542893421
Epoch: 00034  - train loss: 0.0545633619 - train accuracy: 0.9878227151 - val loss: 0.1464943950 - val accuracy: 0.9555590826
Epoch: 00035  - train loss: 0.0516414449 - train accuracy: 0.9893193646 - val loss: 0.1452819793 - val accuracy: 0.9543687009
Epoch: 00036  - train loss: 0.0496402882 - train accuracy: 0.9899656451 - val loss: 0.1418863637 - val accuracy: 0.9550829299
Epoch: 00037  - train loss: 0.0470405339 - train accuracy: 0.9907819994 - val loss: 0.1437292550 - val accuracy: 0.9556384414
Epoch: 00038  - train loss: 0.0459999520 - train accuracy: 0.9910201027 - val loss: 0.1442490207 - val accuracy: 0.9560352353
Epoch: 00039  - train loss: 0.0438719199 - train accuracy: 0.9912922208 - val loss: 0.1403916503 - val accuracy: 0.9575430521
Epoch: 00040  - train loss: 0.0417420362 - train accuracy: 0.9920745604 - val loss: 0.1408208625 - val accuracy: 0.9576224109
Epoch: 00041  - train loss: 0.0389832918 - train accuracy: 0.9924487227 - val loss: 0.1420074792 - val accuracy: 0.9571462582
Epoch: 00042  - train loss: 0.0369018385 - train accuracy: 0.9938773428 - val loss: 0.1410872246 - val accuracy: 0.9560352353
Epoch: 00043  - train loss: 0.0366841417 - train accuracy: 0.9930950032 - val loss: 0.1400931908 - val accuracy: 0.9566701055
Epoch: 00044  - train loss: 0.0358768334 - train accuracy: 0.9936732542 - val loss: 0.1405360736 - val accuracy: 0.9581779224
Epoch: 00045  - train loss: 0.0330304271 - train accuracy: 0.9944896085 - val loss: 0.1366015212 - val accuracy: 0.9580192048
Epoch: 00046  - train loss: 0.0308156229 - train accuracy: 0.9956461104 - val loss: 0.1360710814 - val accuracy: 0.9577017697
Epoch: 00047  - train loss: 0.0289478278 - train accuracy: 0.9956120956 - val loss: 0.1346109248 - val accuracy: 0.9580985636
Epoch: 00048  - train loss: 0.0284858793 - train accuracy: 0.9952379333 - val loss: 0.1353683660 - val accuracy: 0.9592889453
Epoch: 00049  - train loss: 0.0268467752 - train accuracy: 0.9959862580 - val loss: 0.1343560658 - val accuracy: 0.9596857392
Epoch: 00050  - train loss: 0.0251155912 - train accuracy: 0.9966665533 - val loss: 0.1343611594 - val accuracy: 0.9609554797
Epoch: 00051  - train loss: 0.0252547848 - train accuracy: 0.9964964795 - val loss: 0.1337947327 - val accuracy: 0.9599238156
Epoch: 00052  - train loss: 0.0230699971 - train accuracy: 0.9970407157 - val loss: 0.1337496992 - val accuracy: 0.9595270217
Epoch: 00053  - train loss: 0.0217308085 - train accuracy: 0.9974488928 - val loss: 0.1322361360 - val accuracy: 0.9604793270
Epoch: 00054  - train loss: 0.0206348126 - train accuracy: 0.9974148781 - val loss: 0.1334000692 - val accuracy: 0.9607967622
Epoch: 00055  - train loss: 0.0192758322 - train accuracy: 0.9978230552 - val loss: 0.1334167148 - val accuracy: 0.9604793270
Epoch: 00056  - train loss: 0.0181711645 - train accuracy: 0.9980271438 - val loss: 0.1317646840 - val accuracy: 0.9596857392
Epoch: 00057  - train loss: 0.0175798192 - train accuracy: 0.9981291881 - val loss: 0.1315864800 - val accuracy: 0.9593683041
Epoch: 00058  - train loss: 0.0165865585 - train accuracy: 0.9984353209 - val loss: 0.1298962133 - val accuracy: 0.9603206095
Epoch: 00059  - train loss: 0.0151317244 - train accuracy: 0.9985713800 - val loss: 0.1296707394 - val accuracy: 0.9611141973
Epoch: 00060  - train loss: 0.0144061776 - train accuracy: 0.9988094833 - val loss: 0.1307173534 - val accuracy: 0.9596857392
Epoch: 00061  - train loss: 0.0137356654 - train accuracy: 0.9988094833 - val loss: 0.1295974540 - val accuracy: 0.9599238156
Epoch: 00062  - train loss: 0.0126040747 - train accuracy: 0.9992176605 - val loss: 0.1297999591 - val accuracy: 0.9600825331
Epoch: 00063  - train loss: 0.0118747873 - train accuracy: 0.9992516752 - val loss: 0.1291419674 - val accuracy: 0.9609554797
Epoch: 00064  - train loss: 0.0110859054 - train accuracy: 0.9992176605 - val loss: 0.1298487461 - val accuracy: 0.9608761209
Epoch: 00065  - train loss: 0.0109729049 - train accuracy: 0.9992856900 - val loss: 0.1303279753 - val accuracy: 0.9603999683
Epoch: 00066  - train loss: 0.0104168746 - train accuracy: 0.9993877343 - val loss: 0.1304159703 - val accuracy: 0.9607967622
Epoch: 00067  - train loss: 0.0100728977 - train accuracy: 0.9994217490 - val loss: 0.1303634263 - val accuracy: 0.9598444568

```

As one can see, there is an increase tendency on validation prediction accuracy. Early stopping broke training in 67th epoch to prevent overfitting.

### Second architecture

```python

mlp.add_layer(784, activation="sigmoid")
mlp.add_layer(392, activation="sigmoid")
mlp.add_layer(392, activation="sigmoid")


mlp.train(
    X=X_train,
    Y=Y_train,
    bias=True,
    batch_size=64,
    learning_rate=0.005,
    momentum_rate=0.8,
  #   iterations=100,
    mode="multiple_classification",
    loss="categoric_crossentropy",
    epochs=100,
    verbose=True,
    early_stopping=True,
    patience=3,
    validation_split=0.7
)
```

Adding one layer of sigmoid didn't make an improvement on validation set. Best epoch:

<code>
Epoch: 00045  - train loss: 0.0915538806 - train accuracy: 0.9755433858 - val loss: 0.1561820371 - val accuracy: 0.9523847314
</code>

### Other architectures

Similiar architectures with tanh and mixed activation functions didn't improve accuracy on validation set. Then the first architecture were chosen for prediction on test set.

[Submission](https://www.kaggle.com/michalinho/michal-dyczko-digit-recognizer)

Public Score: 0.96142