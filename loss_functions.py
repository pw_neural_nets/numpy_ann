import numpy as np

eps = 1e-15


class LossFunctions:
    """
    Funkcje błędu
    """

    def load(function_name):
        if function_name == "binary_crossentropy":
            loss_function = LossFunctions.binary_crossentropy_derivative
        elif function_name == "categorical_crossentropy":
            loss_function = LossFunctions.categorical_crossentropy_derivative
        elif function_name == "mean_squared_error":
            loss_function = LossFunctions.mean_squared_error_derivative
        elif function_name == "root_mean_squared_error":
            loss_function = LossFunctions.root_mean_squared_error_derivative
        else:
            raise Exception(f"Loss function is not supported: {self.loss}")
        return loss_function

    def binary_crossentropy(Y_hat, Y):
        m = Y_hat.shape[1]
        loss_value = -1 / m * (np.dot(Y, np.log(Y_hat + eps).T) + np.dot(1 - Y, np.log(1 - Y_hat + eps).T))
        return loss_value

    def binary_crossentropy_derivative(Y_hat, Y):
        m = Y_hat.shape[1]
        loss_value = -(np.divide(Y, Y_hat + eps) - np.divide(1 - Y, 1 - Y_hat + eps))
        return loss_value

    def categorical_crossentropy(Y_hat, Y):
        m = Y_hat.shape[1]
        loss_value = (-1 / m) * np.sum(Y * np.log(Y_hat + eps))
        return loss_value

    def categorical_crossentropy_derivative(Y_hat, Y):
        loss_value = -np.divide(Y, Y_hat + eps)
        return loss_value

    def mean_squared_error(Y_hat, Y):
        loss_value = np.mean((Y_hat - Y)**2)
        return loss_value

    def mean_squared_error_derivative(Y_hat, Y):
        loss_value = 2 * (Y_hat - Y)
        return loss_value

    def root_mean_squared_error(Y_hat, Y):
        loss_value = np.sqrt(np.mean((Y_hat - Y)**2))
        return loss_value

    def root_mean_squared_error_derivative(Y_hat, Y):
        loss_value = (Y_hat - Y) / np.sqrt(np.mean((Y_hat - Y)**2))
        return loss_value